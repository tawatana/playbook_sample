playbook-test
=========
playbookのサンプル実装

Requirements
------------
以下に記載
```
playbook_sample/roles/requirements.yml
playbook_sample/collections/requirements.yml
```
インストールは以下コマンド
```
git clone https://gitlab.com/tawatana/playbook_sample && cd playbook_sample
ansible-galaxy role install -fr roles/requirements.yml -p ./roles
ansible-galaxy collection install -fr collections/requirements.yml -p ./collections/

```
Execute playbook
----------------

```
ansible-playbook site.yml -i inventory.yml 
```